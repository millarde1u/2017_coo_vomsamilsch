package Application;

/** Classe Monstre qui herite de la classe abstraite Personnage. Cette classe ermet de creer des monstres.
 *
 * @author S_Camille
 * @author GwendolynV
 * @author millarde1u
 * @author Maxime57
 */
public class Monstre extends Personnage{
	
	/** Constructeur de la classe Monstre qui cree un monstre a une position (x,y) et avec des pv
	 *
	 * @param x
	 * 		Position du monstre en abscisse dans le labyrinthe
	 * @param y
	 * 		Position du monstre en ordonnee dans le labyrinthe
	 */
	public Monstre(int x, int y){
		this.x=x;
		this.y=y;
		this.pv = 2;
	}
	
	@Override
	/** Methode attaquer qui permet aux monstres d'attaquer des personnages
	 *
	 * @param p
	 * 		Personnage que l'on va attaquer
	 */
	public void attaquer(Personnage p) {
		if (!p.etreMort()){
			p.subirDegats(1);
		}
	}

	@Override
	/**
	 * Methode subirDegats qui permet de retirer des pv aux pv du monstre
	 *
	 * @param d
	 * 		Le nombre de pv que l'on va enlever au monstre suite a une attaque sur lui
	 */
	public void subirDegats(int d){
		this.pv = this.pv - d;
		if (this.pv <= 0){
			this.pv = 0;
			this.etreMort();
		}
	}

	/**
	 * Methode getTypeMonstre qui retourne la chaine "Monstre"
	 *
	 * @return la chaine "Monstre"
	 */
	public String getTypeMonstre(){
		return "Monstre";
	}

}