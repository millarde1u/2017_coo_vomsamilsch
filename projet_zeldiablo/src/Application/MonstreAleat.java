package Application;

/** Classe MonstreAleat qui herite de la classe Monstre. Cette classe cree des monstres aleatoires qui se deplacent aleatoirement
 *
 * @author GwendolynV
 * @author S_Camille
 * @author millarde1u
 * @author Maxime57
 */
public class MonstreAleat extends Monstre{

	/** Constructeur de la classe MonstreAleat
	 *
	 * @param x
	 *      Position du monstre aleatoire en abscisse dans le labyrinthe
	 * @param y
	 *      Position du monstre aleatoire en ordonnee dans le labyrinthe
	 */
	public MonstreAleat(int x, int y) {
		super(x, y);
	}

	/** Methode seDeplacer qui permet au monstre aleatoire de se deplacer selon une direction qui lui est donnee
	 *
	 * @param dir
	 *      Direction donnee au monstre aleatoire pour qu'il se deplace
	 */
	public void seDeplacer(String dir){
		if(dir.equals("E")){
			this.x=x+1;
		}
		if(dir.equals("O")){
			this.x=x-1;
		}
		if(dir.equals("S")){
			this.y=y+1;
		}
		if(dir.equals("N")){
			this.y=y-1;
		}
	}

	/**
	 * Methode getTypeMonstre qui retourne la chaine "Monstre Aleatoire"
	 *
	 * @return la chaine "Monstre Aleatoire"
	 */
	public String getTypeMonstre(){
		return "Monstre Aleatoire";
	}

}