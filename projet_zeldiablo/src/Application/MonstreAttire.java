package Application;

/** Classe MonstreAttire qui herite de la classe Monstre. Cette classe cree des monstres attires par le personnage
 *
 * @author GwendolynV
 * @author S_Camille
 * @author millarde1u
 * @author Maxime57
 */
public class MonstreAttire extends Monstre{

	/** Constructeur de la classe MonstreAttire
     *
     * @param x
     *      Position du monstre attire en abscisse dans le labyrinthe
     * @param y
     *      Position du monstre attire en ordonnee dans le labyrinthe
     */
	public MonstreAttire(int x, int y) {
		super(x, y);
	}

	/** Methode seDeplacer qui permet au monstre attire de se deplacer selon une direction qui lui est donnee
     *
     * @param dir
     *      Direction donnee au monstre attire pour qu'il se deplace
     */
	public void seDeplacer(String dir){
		if(dir.equals("E")){
			this.x=x+1;
		}
		if(dir.equals("O")){
			this.x=x-1;
		}
		if(dir.equals("S")){
			this.y=y+1;
		}
		if(dir.equals("N")){
			this.y=y-1;
		}
	}

    /**
     * Methode getTypeMonstre qui retourne la chaine "Monstre Attire"
     *
     * @return la chaine "Monstre Attire"
     */
	public String getTypeMonstre(){
		return "Monstre Attire";
	}

}