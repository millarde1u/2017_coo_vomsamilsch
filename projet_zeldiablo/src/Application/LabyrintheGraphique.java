package Application;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.Font;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import MoteurJeu.DessinJeu;

/** Classe LabyrintheGraphique qui implemente l'interface DessinJeu. Cette classe cree le labyrinthe sous forme graphique.
 *
 * @author GwendolynV
 * @author S_Camille
 * @author millarde1u
 * @author Maxime57
 */
public class LabyrintheGraphique implements DessinJeu{

	/** Labyrinthe que l'on va dessiner */
	private Labyrinthe lab;

	/** Constructeur de la classe LabyrintheGraphique
     *
     * @param lb
     *      Labyrinthe que l'on va dessiner*/
	public LabyrintheGraphique(Labyrinthe lb){
		this.lab=lb;
	}
	
	@Override
	/** Methode dessiner qui construit une image correspondant au jeu
     *
     * @param image
     *      Image sur laquelle on va dessiner
     */
	public void dessiner(BufferedImage image) {
		int k = 0;
		java.awt.Graphics g = image.getGraphics();
		int x = image.getHeight();
		int y = image.getWidth();
		ArrayList<Monstre> monstre = this.lab.getListeMonstreProche();
		int nbCase = this.lab.getTaille();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, nbCase*50, nbCase*50);
		g.setColor(Color.GRAY);
		g.fillRect(this.lab.getDepart().getX()*50, this.lab.getDepart().getY()*50,50,50);
		for(int i=0; i<nbCase; i++){
			for(int j=0; j<nbCase; j++){
				if(this.lab.getAventurier().getX()==j && this.lab.getAventurier().getY()==i){
					Image imageFond = null;
					try {
						imageFond = ImageIO.read(new File("hero.png"));
					} catch (IOException e) {
						e.printStackTrace();
					}
					g.drawImage(imageFond, (j*50)+5, (i*50)+5, null);
					if(this.lab.getAventurier().possedeAmulette()){
						g.setColor(Color.YELLOW);
						g.fillOval(((j*50)+27), ((i*50)+27), 6, 6);
					}
				}
				else if(this.lab.getCases()[i][j] instanceof CaseMur){
					Image imageFond = null;
					try {
						imageFond = ImageIO.read(new File("Mur.jpg"));
					} catch (IOException e) {
						e.printStackTrace();
					}
					g.drawImage(imageFond, (j*50), (i*50), null);
				}
				else if(this.lab.getCases()[i][j] instanceof CaseVide) {
					g.setColor(Color.BLACK);
					g.drawRect((j * 50), (i * 50), 50, 50);
				}
				if (this.lab.getTabObjet()[j][i] != null) {
					g.setColor(Color.YELLOW);
					g.fillOval(((j*50)+50/2), ((i*50)+50/2), 10, 10);
				}
			}
		}
		for(Monstre n:this.lab.getListeMonstre()){
			if(n.getTypeMonstre().equals("Monstre Attire")){
				Image imageFond = null;
				try {
					imageFond = ImageIO.read(new File("fant.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				g.drawImage(imageFond, (n.getX()*50), (n.getY()*50), null);
			}else if(n.getTypeMonstre().equals("Monstre Aleatoire")){
				Image imageFond = null;
				try {
					imageFond = ImageIO.read(new File("aleat.jpg"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				g.drawImage(imageFond, (n.getX()*50), (n.getY()*50), null);
			}
			else{
				Image imageFond = null;
				try {
					imageFond = ImageIO.read(new File("immo.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				g.drawImage(imageFond, (n.getX()*50), (n.getY()*50), null);
			}
			k++;
		}
		g.setColor(Color.BLUE);
		g.drawRect(0, 500, 500, 50);
		Font f = new Font(" TimesRoman ", Font.BOLD, 30);
		g.setFont(f);
		g.drawString("PV : " + this.lab.getAventurier().getPv(), 100, 530);
		this.lab.evolutionJeu();
		if (this.lab.etreFini()) {
			if (this.lab.getAventurier().etreMort() == true) {
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, nbCase * 50, nbCase * 50);
				Font fonte = new Font(" TimesRoman ", Font.BOLD, 30);
				g.setFont(fonte);
				g.setColor(Color.GREEN);
				g.drawString("GAME OVER", 100, 100);
			} else {
				g.setColor(Color.BLACK);
				g.fillRect(0, 0, nbCase * 50, nbCase * 50);
				Font fonte = new Font(" TimesRoman ", Font.BOLD, 30);
				g.setFont(fonte);
				g.setColor(Color.PINK);
				g.drawString("Vous Avez Gagne !!", 125, 250);
			}
		}
	}

}