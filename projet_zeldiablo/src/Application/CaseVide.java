package Application;

/**
 * Classe CaseVide qui implemente l'interface CaseLab. Cette classe permet la creation de cases vides.
 *
 * @author S_Camille
 * @author GwendolynV
 * @author millarde1u
 * @author Maxime57
 */
public class CaseVide implements CaseLab{

	/** Abscisse de la case */
	private int positionX;
	/** Ordonnee de la case */
	private int positionY;
	
	/**
	 * Constructeur de la classe CaseVide, qui cree une case vide
	 *
	 * @param x
	 * 		Abscisse de la case
	 * @param y
	 * 		Ordonnee de la case
	 */
	public CaseVide(int x, int y){
		this.positionX = x;
		this.positionY = y;
	}
	
	/**
	 * Methode getX qui retourne l'abscisse de la case
	 *
	 * @return l'abscisse de la case
	 */
	public int getX(){
		return this.positionX;
	}

	/**
	 * Methode getY qui retourne l'ordoneee de la case
	 *
	 * @return l'ordonnee de la case
	 */
	public int getY(){
		return this.positionY;
	}
	
	/**
	 * Methode getTypeCase qui retourne la chaine "Case Vide"
	 *
	 * @return la chaine "Case Vide"
	 */
	public String getTypeCase(){
		return "Case Vide";
	}

}