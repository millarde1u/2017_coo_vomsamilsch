package Application;

import java.util.ArrayList;
import MoteurJeu.Commande;
import MoteurJeu.Jeu;
/** Classe Labyrinthe qui implemente l'interface jeu. Cette classe cree un labyrinthe dans lequel l'aventurier et les montres se cotoient.
 *
 * @author GwendolynV
 * @author S_Camille
 * @author millarde1u
 * @author Maxime57
 */
public class Labyrinthe implements Jeu{

	/** Liste de monstres */
	private ArrayList<Monstre> liste_Monstre;
	/** Tableau de cases */
	private CaseLab[][] tab_cases;
	/** Case d'entree du labyrinthe */
	private CaseLab depart;
	/** Aventurier present dans le labyrinthe */
	private Aventurier aventurier;
	/** Nombre du cases presentes dans le labyrinthe */
	private int taille;
	/** Tableau d'objets */
	private Objet[][] tab_o;

	/** Constructeur sans parametre de la classe Labyrinthe */
	public Labyrinthe(){
		int nbCases = 10;
		this.liste_Monstre=new ArrayList<Monstre>();
		this.tab_cases = new CaseLab[nbCases][nbCases];
		this.tab_o = new Objet[nbCases][nbCases];
		this.taille = nbCases;
		for(int i=0; i<nbCases;i++){
			for(int j = 0; j<nbCases; j++ ){
				this.tab_cases[i][j] = new CaseVide(i,j);
			}
		}
		for(int i=0; i<2;i++){
			this.tab_cases[0][i] = new CaseMur(0,i);
		}
		for(int i=4; i<nbCases;i++){
			this.tab_cases[i][0] = new CaseMur(i,0);
		}
		this.tab_cases[3][1] = new CaseMur(3,1);
		this.tab_cases[3][2] = new CaseMur(3,2);
		for(int i=2; i<4;i++){
			this.tab_cases[i][4] = new CaseMur(i,4);
		}
		
		this.tab_cases[5][2] = new CaseMur(5,2);
		this.tab_cases[5][3] = new CaseMur(5,3);
		for(int i=2;i<7;i++){
			this.tab_cases[6][i] = new CaseMur(6,i);
		}
		for(int i=0;i<7;i++){
			this.tab_cases[9][i] = new CaseMur(9,i);
		}
		this.tab_cases[8][9] = new CaseMur(8,9);
		this.tab_cases[8][8] = new CaseMur(8,8);
		for(int i=0;i<7;i++){
			this.tab_cases[i][7] = new CaseMur(i,7);
		}
		this.aventurier = new Aventurier(5,5,10);
		this.genererMonstre();
		this.tab_cases[1][4] = new CaseVide(1, 4);
		this.tab_o[1][4] = new Amulette();
	}

	/** Constructeur avec parametre de la classe Labyrinthe
	 *
	 * @param nbCases
	 *		Nombre du cases que l'on veut dans le labyrinthe
	 */
	public Labyrinthe(int nbCases){
		this.liste_Monstre=new ArrayList<Monstre>();
		if (nbCases < 1){
			nbCases = 1;
		}
		this.tab_cases = new CaseLab[nbCases][nbCases];
		this.taille = nbCases;
		this.tab_o = new Objet[nbCases][nbCases];
		this.generationLabyrintheAlea();
		this.tab_cases[(int)nbCases/2][(int)nbCases/2]=new CaseVide((int)nbCases/2,(int)nbCases/2);
		depart = this.tab_cases[(int)nbCases/2][(int)nbCases/2];
		this.aventurier = new Aventurier((int)nbCases/2,(int)nbCases/2, this.taille);
		this.genererMonstre();
		CaseLab c = new CaseVide(1, 4);
		this.tab_cases[1][4] = c;
		this.tab_o[1][4] = new Amulette();
	}

	/** Methode privee genererMonstre qui permet de generer des monstres */
	private void genererMonstre() {

		int posDepX = 0;
		int posDepY = 0;
		boolean trouve = false;
		int nbMonstre = 5;
		for (int k = 0; k < nbMonstre; k++) {
			trouve = false;
			while (!trouve) {
				posDepX = (int) (Math.random() * this.taille - 1);
				posDepY = (int) (Math.random() * this.taille - 1);
				if (this.liste_Monstre.size() != 0) {
					if (this.tab_cases[posDepY][posDepX].getTypeCase().equals("Case Vide")) {
						for (Monstre n : this.liste_Monstre) {
							if (n.getX() != posDepX && n.getY() != posDepY && this.aventurier.getX() != posDepX && this.aventurier.getY() != posDepY) {
								trouve = true;
							} else {
								trouve = false;
							}
						}
					}
				} else if (this.tab_cases[posDepY][posDepX].getTypeCase().equals("Case Vide") && this.aventurier.getX() != posDepX && this.aventurier.getY() != posDepY) {
					trouve = true;
				}
			}
			double r = Math.random();
			if (r < 0.3) {
				this.liste_Monstre.add(new MonstreImmo(posDepX, posDepY));
			} else if (r > 0.3 && r < 0.6) {
				this.liste_Monstre.add(new MonstreAleat(posDepX, posDepY));
			} else if (r>0.6){
				this.liste_Monstre.add(new MonstreAttire(posDepX, posDepY));
			}
		}
	}

	/** Methode privee generationLabyrintheAlea qui permet de generer un labyrinthe aleatoirement */
	private void generationLabyrintheAlea(){
		if (this.tab_cases.length>1){
			for(int i=0; i<this.tab_cases.length;i++){
				for(int j = 0; j<this.tab_cases[0].length; j++ ){
					double test = Math.random();
					if (test < 0.6 ||( i==4 && j == 1)){
						this.tab_cases[i][j] = new CaseVide(i,j);
					}
					else{
						this.tab_cases[i][j] = new CaseMur(i,j);
					}
				}
			}
		}
		else{
			this.tab_cases[0][0] = new CaseVide(0,0);
		}
	}

	/** Methode getTaille qui retourne la taille du labyrinthe
	 *
	 * @return la taille du labyrinthe
	 */
	public int getTaille(){
		return this.taille;
	}

	/** Methode getAventurier qui retourne un aventurier
	 *
	 * @return un aventurier
	 */
	public Aventurier getAventurier(){
		return this.aventurier;
	}

	/** Methode getCases qui retourne le tableau de cases
	 *
	 * @return le tableau de cases
	 */
	public CaseLab[][] getCases(){
		return this.tab_cases;
	}

	/** Methode getDepart qui retourne la case d'entree du labyrinthe
	 *
	 * @return la case d'entree du labyrinthe
	 */
	public CaseLab getDepart(){
		return this.depart;
	}

	/** Methode getListeMonstre qui retourne la liste de monstres
	 *
	 * @return la liste de monstres
	 */
	public ArrayList<Monstre> getListeMonstre(){
		return this.liste_Monstre;
	}
	
	@Override
	/** Methode evoluer qui permet de faire evoluer le jeu en fonction d'une commande saisie par l'utilisateur
	 *
	 * @param commandeUser
	 * 		Commande saisie par l'utilisateur
	 */
	public void evoluer(Commande commandeUser) {
		if(commandeUser.bas){
			this.deplacementPersonnage("s");
		}
		else if(commandeUser.droite){
			this.deplacementPersonnage("e");
		}
		else if(commandeUser.gauche){
			this.deplacementPersonnage("o");
		}
		else if(commandeUser.haut){
			this.deplacementPersonnage("n");
		}
		else if(commandeUser.espace){
			for(int i=0; i<this.liste_Monstre.size();i++){
				Monstre n = this.liste_Monstre.get(i);
				if(this.getDistancePersonnage(aventurier,n) <= 3) {
					aventurier.attaquer(n);
					if (n.etreMort()) {
						this.liste_Monstre.remove(n);
					}
				}
			}
		}
		else if (commandeUser.bombe){
			if (aventurier.getBombe()){
				for(int i=this.aventurier.getY()-1; i<this.aventurier.getY()+1; i++){
					for(int j=this.aventurier.getX()-1; j<this.aventurier.getX()+1; j++){
						//if (i<this.tab_cases.length && j<this.tab_cases[0].length && i>=-1 && j>= -1) {
							this.tab_cases[j][i] = new CaseVide(i, j);
						//}
					}
				}
				this.aventurier.utiliserBombe();
			}
		}
	}

	@Override
	/** Methode etreFini qui retourne true, si et seulement si le jeu est fini
	 *
	 * @return  true, si et seulement si le jeu est fini
	 */
	public boolean etreFini() {
		boolean fini = false;
		if (aventurier.deplacementPossible() == false || this.aventurier.possedeAmulette() && this.aventurier.getX() == this.depart.getX() && this.aventurier.getY() == this.depart.getY() || this.getAventurier().etreMort()) {
			fini = true;
		}
		return fini;

	}

	/** Methode ajoutElementInventaire qui permet d'ajouter un objet a l'inventaire de l'aventurier qui est dans le labyrinthe
	 *
	 * @param o
	 *      Objet que l'on veut ajouter a l'inventaire de l'aventurier qui est dans le labyrinthe
	 */
	public void ajoutElementInventaire(Objet o){
		this.aventurier.ajoutElementInventaire(o);
	}

	/** Methode afficheContenuInventaire qui permet d'afficher le contenu de l'inventaire de l'aventurier qui est dans le labyrinthe */
	public void afficheContenuInventaire(){
		this.aventurier.afficheContenuInventaire();
	}

	/** Methode getListeMonstreProche qui retourne une liste de monstres qui sont proches de l'aventurier
	 *
	 * @return une liste de monstres qui sont proches de l'aventurier
	 */
	public ArrayList<Monstre> getListeMonstreProche(){
		ArrayList<Monstre> tampon = new ArrayList<Monstre>();
		for(Monstre m:this.liste_Monstre){
			if((Math.abs(this.aventurier.getX()-m.getX()) == 1 && Math.abs(this.aventurier.getY()-m.getY()) == 0) || (Math.abs(this.aventurier.getX()-m.getX()) == 0 && Math.abs(this.aventurier.getY()-m.getY()) == 1 )){
				tampon.add(m);
			}
		}
		return tampon;
	}

	/** Methode getTabObjet qui retourne le tableau d'objets
	 *
	 * @return le tableau d'objets
	 */
	public Objet[][] getTabObjet() {
		return this.tab_o;
	}

	/** Methode deplacementPersonnage qui permet de deplacer le personnage en fonction d'une chaine saisie
	 *
	 * @param ch
	 * 		Chaine saisie qui correspond au deplacement que fera le personnage
	 */
	public void deplacementPersonnage(String ch) {
		switch(ch) {
			case "n":
				if (aventurier.getY() - 1 >= 0 && this.tab_cases[aventurier.getY() - 1][aventurier.getX()].getTypeCase().equals("Case Vide")) {
					aventurier.setY(aventurier.getY() - 1);
				}
				break;
			case "s":
				if (aventurier.getY() + 1 < this.tab_cases.length && this.tab_cases[aventurier.getY() + 1][aventurier.getX()].getTypeCase().equals("Case Vide")) {
					aventurier.setY(aventurier.getY() + 1);
				}
				break;
			case "e":
				if (aventurier.getX() + 1 < this.tab_cases.length) {
					if (this.tab_cases[aventurier.getY()][aventurier.getX() + 1].getTypeCase().equals("Case Vide")) {
						aventurier.setX(aventurier.getX() + 1);
					}
				}
				break;
			case "o":
				if (aventurier.getX() - 1 >= 0) {
					if (this.tab_cases[aventurier.getY()][aventurier.getX() - 1].getTypeCase().equals("Case Vide")) {
						aventurier.setX(aventurier.getX() - 1);
					}
				}
				break;
		}
		if (aventurier.getX() == 1 && aventurier.getY() == 4){
			if (this.tab_o[1][4] != null) {
				aventurier.ajoutAmulette((Amulette) this.tab_o[1][4]);
				this.tab_o[1][4] = null;
				aventurier.afficheContenuInventaire();
			}
		}
	}

	/** Methode evolutionJeu qui permet de gerer le comportement des monstres quand le jeu se rafraichit */
	public void evolutionJeu() {
		ArrayList<Monstre> lm = this.getListeMonstreProche();
		for(Monstre n:lm){
			n.attaquer(this.aventurier);
		}
		for(Monstre n:this.getListeMonstre()){
			if(n.getTypeMonstre().equals("Monstre Aleatoire")){
				boolean trouve = false;
				for(Monstre m:lm){
					if(m==n){
						trouve=true;
					}
				}
				if(trouve==false){
					double r = (Math.random());
					if(r<0.25){
						if(n.getX()+1<this.taille){
							if(this.tab_cases[n.getY()][n.getX()+1].getTypeCase().equals("Case Vide")&&this.aventurier.getX()!=n.getX()+1){
								((MonstreAleat)n).seDeplacer("E");
							}
						}
					}
					else if(r<0.5&&r>0.25){
						if(n.getX()-1>=0){
							if(this.tab_cases[n.getY()][n.getX()-1].getTypeCase().equals("Case Vide")&&this.aventurier.getX()!=n.getX()-1){
								((MonstreAleat)n).seDeplacer("O");
							}
						}
					}
					else if(r>0.5&&r<0.75){
						if(n.getY()+1<this.taille){
							if(this.tab_cases[n.getY()+1][n.getX()].getTypeCase().equals("Case Vide")&&this.aventurier.getY()!=n.getY()+1){
								((MonstreAleat)n).seDeplacer("S");
							}
						}
					}
					else if(r>0.75){
						if(n.getY()-1>=0){
							if(this.tab_cases[n.getY()-1][n.getX()].getTypeCase().equals("Case Vide")&&this.aventurier.getY()!=n.getY()-1){
								((MonstreAleat)n).seDeplacer("N");
							}
						}
					}
				}
			}
			if(n.getTypeMonstre().equals("Monstre Attire")){
				int dx=this.aventurier.getX()-n.getX();
				int dy=this.aventurier.getY()-n.getY();
				int dxAbs=Math.abs(dx);
				int dyAbs=Math.abs(dy);
				if(dxAbs>dyAbs){
					if(dx>0){
						if(n.getX()+1<this.taille){
							if(this.tab_cases[n.getY()][n.getX()+1].getTypeCase().equals("Case Vide")&&this.aventurier.getX()!=n.getX()+1){
								((MonstreAttire)n).seDeplacer("E");
							}
						}
					}
					else{
						if(n.getX()-1>=0){
							if(this.tab_cases[n.getY()][n.getX()-1].getTypeCase().equals("Case Vide")&&this.aventurier.getX()!=n.getX()-1){
								((MonstreAttire)n).seDeplacer("O");
							}
						}
					}
				}else{
					if(dy>0){
						if(n.getY()+1<this.taille){
							if(this.tab_cases[n.getY()+1][n.getX()].getTypeCase().equals("Case Vide")&&this.aventurier.getY()!=n.getY()+1){
								((MonstreAttire)n).seDeplacer("S");
							}
						}
					}else{
						if(n.getY()-1>=0){
							if(this.tab_cases[n.getY()-1][n.getX()].getTypeCase().equals("Case Vide")&&this.aventurier.getY()!=n.getY()-1){
								((MonstreAttire)n).seDeplacer("N");
							}
						}
					}
				}
			}
		}
	}

	private int getDistancePersonnage(Personnage p1, Personnage p2){
		return (Math.abs(p1.getX()-p2.getX())+Math.abs(p1.getY()-p2.getY()));
	}

}