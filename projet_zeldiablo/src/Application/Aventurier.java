package Application;

/** Classe Aventurier qui herite de la classe abstraite Personnage. Cette classe va permettre de creer des aventuriers.
 *
 * @author GwendolynV
 * @author S_Camille
 * @author millarde1u
 * @author Maxime57
 */
public class Aventurier extends Personnage{

	/** Taille du labyrinthe */
	private int tailleLab;
	/** */
	private boolean deplacement;
	/** Inventaire */
	private Inventaire inventaire;
	/** */
	private boolean attaque;
	/** Amulette */
	private Amulette amulette;
	/** Bombe */
	private boolean bombe;

	/** Constructeur de classe Aventurier
	 *
	 * @param ax
	 * 		Position de l'aventurier en abscisse dans le labyrinthe
	 * @param ay
	 * 		Position de l'aventurier en ordonnee dans le labyrinthe
	 * @param taille
	 * 		 Taille du labyrinthe
	 */
	public Aventurier(int ax, int ay, int taille) {
		this.x = ax;
		this.y = ay;
		this.tailleLab = taille;
		this.deplacement = true;
		this.pv = 5;
		this.mort = false;
		this.inventaire = new Inventaire();
		this.attaque = false;
		this.amulette = null;
		this.bombe = true;
	}

	/** Methode deplacementPossible qui retourne false si le deplacement de l'aventurier est impossible
	 *
	 * @return false si le deplacement de l'aventurier est impossible
	 */
	public boolean deplacementPossible() {
		if(y==this.tailleLab || x == this.tailleLab){
			deplacement = false;
		}
		return deplacement;
	}

	/** Methode ajoutElementInventaire qui permet d'ajouter un objet a l'inventaire de l'aventurier
	 *
	 * @param o
	 *      Objet que l'on veut ajouter a l'inventaire
	 */
	public void ajoutElementInventaire(Objet o){
		this.inventaire.ajoutElementInventaire(o);
	}

	/** Methode afficheContenuInventaire qui permet d'afficher le contenu de l'inventaire de l'aventurier */
	public void afficheContenuInventaire(){
		this.inventaire.afficheContenuInventaire();
	}
	
	@Override
	/** Methode attaquer qui permet aux aventuriers d'attaquer des personnages
	 *
	 * @param p
	 * 		Personnage que l'on va attaquer
	 */
	public void attaquer(Personnage p) {
		if (p!=null) {
			if (!p.etreMort()) {
				p.subirDegats(1);
			}
		}
	}

	@Override
	/**
	 * Methode subirDegats qui permet de retirer des pv aux pv de l'aventurier
	 *
	 * @param d
	 * 		Le nombre de pv que l'on va enlever a l'aventurier suite a une attaque sur lui
	 */
	public void subirDegats(int d){
		this.pv = this.pv - d;
		System.out.println("pv : " + this.getPv());
		if (this.pv <= 0){
			this.pv = 0;
			this.etreMort();
		}
	}

	/** Methode */
	public Objet[] getElement(){
		return inventaire.getElement();
	}

	/** Methode ajoutAmulette qui permet d'ajouter une amulette a l'aventurier et de la mettre dans l'inventaire
	 *
	 * @param amu
	 * 		Amulette que l'on veut ajouter a l'aventurier
	 */
	public void ajoutAmulette(Amulette amu) {
		this.amulette = amu;
	}

	/** Methode possedeAmulette qui retourne true si
	 *
	 * @return true si le hero possede l'amulette
	 */
	public boolean possedeAmulette(){
		return !(this.amulette==null);
	}

	/** Methode getBombe qui retourne si la bombe a été utilisée
	 *
	 * @return true si le hero possede encore la bombe
	 */


	public boolean getBombe(){
		return bombe;
	}

	/** Methode utiliserBomobe qui permet de passer la bombe la a utiliser
	 */


	public void utiliserBombe(){
		this.bombe = false;
	}
}