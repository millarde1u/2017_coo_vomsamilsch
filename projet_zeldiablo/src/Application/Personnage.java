package Application;

/** Classe abstraite Personnage qui va permettre de creer des personnages
 *
 * @author GwendolynV
 * @author S_Camille
 * @author millarde1u
 * @author Maxime57
 */
public abstract class Personnage {

	/** Position du personnage en abscisse dans le labyrinthe */
	protected int x;
	/** Position du personnage en ordonnee dans le labyrinthe */
	protected int y;
	/** Nombre de points de vie du joueur */
	protected int pv;
	/** Booleen retournant true si le personnage est mort */
	protected boolean mort;

	/** Constructeur vide de la classe Personnage */
	public Personnage(){}

	/** Methode abstraite attaquer qui permettra l'attaque sur des personnages
	 *
	 * @param p
	 * 		Personnage que l'on va attaquer
	 */
	public abstract void attaquer(Personnage p);
	
	/**
	 * Methode abstraite subirDegats qui permet de retirer des pv aux pv du personnage
	 *
	 * @param d
	 * 		Le nombre de pv que l'on souhaite enlever au personnage
	 */
	public abstract void subirDegats(int d);

	/**
	 * Methode getX qui retourne la position du personnage en abscisse dans le labyrinthe
	 *
	 * @return la posiition du personnage en abscisse dans le labyrinthe
	 */
	public int getX(){
		return this.x;
	}

	/**
	 * Methode getY qui retourne la position du personnage en ordonnee dans le labyrinthe
	 *
	 * @return la posiition du personnage en ordonnee dans le labyrinthe
	 */
	public int getY(){
		return this.y;
	}

	/**
	 * Methode getPv qui retourne le nombre de pv du personnage
	 *
	 * @return le nombre de pv du personnage
	 */
	public int getPv(){
		return this.pv;
	}

	/** Methode setX qui permet de modifier la position du personnage en abscisse dans le labyrinthe
	 *
	 * @param nx
	 * 		Nouvelle position du personnage en abscisse dans le labyrinthe
	 */
	public void setX(int nx) {
		this.x = nx;
	}

	/** Methode setY qui permet de modifier la position du personnage en ordonnee dans le labyrinthe
	 *
	 * @param ny
	 * 		Nouvelle position du personnage en ordonnee dans le labyrinthe
	 */
	public void setY(int ny) {
		this.y = ny;
	}

	/** Methode deplacement qui permet de modifier la position du personnage en abscisse et en ordonnee
	 * dans le labyrinthe
	 *
	 * @param nx
	 * 		Nouvelle position du personnage en abscisse dans le labyrinthe
	 * @param ny
	 * 		Nouvelle position du personnage en ordonnee dans le labyrinthe
	 */
	public void deplacement(int nx, int ny){
		this.x = nx;
		this.y = ny;
	}

	/** Methode etreMort qui retourne true si le personnage n'a plus de pv, et est donc mort
	 *
	 * @return true si le personnage n'a plus de pv, et est donc mort
	 */
	public boolean etreMort() {
		if (this.pv == 0) {
			this.mort = true;
		}
		return mort;
	}

}