package Application;

/** Interface Objet*/
public interface Objet {

    /** Methode afficheElement qui permet l'affichage de l'objet dans la console */
    public void afficheElement();

    /** Methode getNom qui retourne le nom de l'objet
     *
     * @return le nom de l'objet
     */
    public String getNom();

    /** Methode qui retourne l'objet lui-meme
     *
     * @return l'objet lui-meme*/
    public Objet getObjet();
}
