package Application;

/** Classe MonstreImmo qui herite de la classe Monstre. Cette classe cree des monstres immobiles dans le labyrinthe
 *
 * @author GwendolynV
 * @author S_Camille
 * @author millarde1u
 * @author Maxime57
 */
public class MonstreImmo extends Monstre{

	/** Constructeur de la classe MonstreImmo
	 *
	 * @param x
	 *      Position du monstre immobile en abscisse dans le labyrinthe
	 * @param y
	 *      Position du monstre immobile en ordonnee dans le labyrinthe
	 */
	public MonstreImmo(int x, int y) {
		super(x, y);
	}

	/**
	 * Methode getTypeMonstre qui retourne la chaine "Monstre Immobile"
	 *
	 * @return la chaine "Monstre Immobile"
	 */
	public String getTypeMonstre(){
		return "Monstre Immobile";
	}

}