package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Application.Labyrinthe;
import Application.MonstreAttire;

public class TestMonstreDeplacementTest {

	@Test
	public void testDeplacementNord() {
		Labyrinthe lab = new Labyrinthe();
		int posDepX=0;
		int posDepY=0;
		for(int i =0; i<lab.getTaille();i++){
			for(int j = 0; j<lab.getTaille();j++){
				if(lab.getCases()[i][j].getTypeCase().equals("Case Vide")&&i+1<lab.getTaille());{
					posDepY=i+1;
					posDepX=j;
				}
			}
		}
		MonstreAttire m = new MonstreAttire(posDepX,posDepY);
		m.seDeplacer("N");
		
		assertEquals("Deplacement corrompus", posDepY-1, m.getY());
	}
	
	@Test
	public void testDeplacementSud() {
		Labyrinthe lab = new Labyrinthe();
		int posDepX=0;
		int posDepY=0;
		for(int i =0; i<lab.getTaille();i++){
			for(int j = 0; j<lab.getTaille();j++){
				if(lab.getCases()[i][j].getTypeCase().equals("Case Vide")&&i-1>=0);{
					posDepY=i-1;
					posDepX=j;
				}
			}
		}
		MonstreAttire m = new MonstreAttire(posDepX,posDepY);
		m.seDeplacer("S");
		
		assertEquals("Deplacement corrompus", posDepY+1, m.getY());
	}
	
	@Test
	public void testDeplacementEst() {
		Labyrinthe lab = new Labyrinthe();
		int posDepX=0;
		int posDepY=0;
		for(int i =0; i<lab.getTaille();i++){
			for(int j = 0; j<lab.getTaille();j++){
				if(lab.getCases()[i][j].getTypeCase().equals("Case Vide")&&j-1>=0);{
					posDepY=i;
					posDepX=j-1;
				}
			}
		}
		MonstreAttire m = new MonstreAttire(posDepX,posDepY);
		m.seDeplacer("E");
		
		assertEquals("Deplacement corrompus", posDepX+1, m.getY());
	}
	
	@Test
	public void testDeplacementOuest() {
		Labyrinthe lab = new Labyrinthe();
		int posDepX=0;
		int posDepY=0;
		for(int i =0; i<lab.getTaille();i++){
			for(int j = 0; j<lab.getTaille();j++){
				if(lab.getCases()[i][j].getTypeCase().equals("Case Vide")&&j+1<lab.getTaille());{
					posDepY=i;
					posDepX=j+1;
				}
			}
		}
		MonstreAttire m = new MonstreAttire(posDepX,posDepY);
		m.seDeplacer("O");
		
		assertEquals("Deplacement corrompus", posDepX-1, m.getY());
	}
	
	@Test
	public void testDeplacementNordMur() {
		Labyrinthe lab = new Labyrinthe();
		int posDepX=0;
		int posDepY=0;
		for(int i =0; i<lab.getTaille();i++){
			for(int j = 0; j<lab.getTaille();j++){
				if(lab.getCases()[i][j].getTypeCase().equals("Case Mur")&&i+1>=0);{
					posDepY=i+1;
					posDepX=j;
				}
			}
		}
		MonstreAttire m = new MonstreAttire(posDepX,posDepY);
		m.seDeplacer("N");
		
		assertEquals("Deplacement corrompus", posDepY-1, m.getY());
	}
	
	@Test
	public void testDeplacementSudMur() {
		Labyrinthe lab = new Labyrinthe();
		int posDepX=0;
		int posDepY=0;
		for(int i =0; i<lab.getTaille();i++){
			for(int j = 0; j<lab.getTaille();j++){
				if(lab.getCases()[i][j].getTypeCase().equals("Case Mur")&&i-1<lab.getTaille());{
					posDepY=i-1;
					posDepX=j;
				}
			}
		}
		MonstreAttire m = new MonstreAttire(posDepX,posDepY);
		m.seDeplacer("S");
		
		assertEquals("Deplacement corrompus", posDepY+1, m.getY());
	}
	
	@Test
	public void testDeplacementEstMur() {
		Labyrinthe lab = new Labyrinthe();
		int posDepX=0;
		int posDepY=0;
		for(int i =0; i<lab.getTaille();i++){
			for(int j = 0; j<lab.getTaille();j++){
				if(lab.getCases()[i][j].getTypeCase().equals("Case Mur")&&j-1>=0);{
					posDepY=i;
					posDepX=j-1;
				}
			}
		}
		MonstreAttire m = new MonstreAttire(posDepX,posDepY);
		m.seDeplacer("E");
		
		assertEquals("Deplacement corrompus", posDepX+1, m.getY());
	}
	
	@Test
	public void testDeplacementOuestMur() {
		Labyrinthe lab = new Labyrinthe();
		int posDepX=0;
		int posDepY=0;
		for(int i =0; i<lab.getTaille();i++){
			for(int j = 0; j<lab.getTaille();j++){
				if(lab.getCases()[i][j].getTypeCase().equals("Case Mur")&&j+1<lab.getTaille());{
					posDepY=i;
					posDepX=j+1;
				}
			}
		}
		MonstreAttire m = new MonstreAttire(posDepX,posDepY);
		m.seDeplacer("O");
		
		assertEquals("Deplacement corrompus", posDepX-1, m.getY());
	}
}
