package Test;
import static org.junit.Assert.*;

import org.junit.Test;

import Application.Labyrinthe;
import Application.Monstre;

public class TestAventurier {

	@Test
	public void testAttaquerMort(){
		Labyrinthe lab = new Labyrinthe();
		Monstre monstre = new Monstre(lab.getAventurier().getX() + 1,lab.getAventurier().getY());
		
		
		lab.getAventurier().attaquer(monstre);
		lab.getAventurier().attaquer(monstre);
		
		
		assertEquals("le monstre est mort", true, monstre.etreMort());
	}
	
	@Test
	public void testAttaquerVivant(){
		Labyrinthe lab = new Labyrinthe();
		Monstre monstre = new Monstre(lab.getAventurier().getX() + 1,lab.getAventurier().getY());
		
		lab.getAventurier().attaquer(monstre);
		
		assertEquals("le monstre est vivant", false, monstre.etreMort());
	}
	
	
	@Test 
	public void testSubirDegat(){
		Labyrinthe lab = new Labyrinthe();
		
		lab.getAventurier().subirDegats(1);

		assertEquals("le heros a perdu un pv", 4, lab.getAventurier().getPv());
		assertEquals("le heros n'est pas mort", false, lab.getAventurier().etreMort());
		
	}
	
	@Test
	public void testSubirDegatMort(){
		Labyrinthe lab = new Labyrinthe();
		
		lab.getAventurier().subirDegats(8);
		
		assertEquals("le heros a zero pv", 0, lab.getAventurier().getPv());
		assertEquals("le heros est mort", true, lab.getAventurier().etreMort());
	}
	
	@Test
	public void testSeDeplacer() {
		
	}

}
