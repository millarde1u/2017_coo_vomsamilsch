@startuml

skinparam class {
	backgroundColor<<Nouv>> Pink
	borderColor<<Nouv>> Black
}
class Aventurier<<Nouv>>{
	- tailleLab : int
	- deplacement : boolean
	+ Aventurier(ax :int, ay : int, taille : int)
	+ setTabCase(cb : CaseLab[][])
	+ seDeplacer(ch : String)
	+ deplacementPossible() : boolean
	+ etreMort() : boolean
	+ ajoutElementInventaire(Objet o)
	+ afficheContenuInventaire()
}

abstract class Personnage<<Nouv>>{
    # x : int
	# y : int
	# pv : int
	# mort : boolean
	+ getX() : int
	+ getY() : int
	+ getPv() : int
	+ setX(nx : int)
	+ setY(ny : int)
	+ subirDegats(int d)
	+ attaquer(Personnage p)
	+ etreMort() : boolean
}

class Labyrinthe<<Nouv>>{
	- tab_cases : CaseLab[][]
	- taille : int
	- finjeu : boolean
	+ Labyrinthe(nbCases : int)
	+ getAventurier() : Aventurier
	+ getCases() : CaseLab[][]
	+ getDepart() : CaseLab[][]
	+ evoluer(Commande)
	+ etreFini() : boolean
	+ endGame()
	+ ajoutElementInventaire(Objet o)
	+ afficheContenuInventaire()
	+ getFJ() : boolean
}

interface CaseLab{
	+ getType() : String
	+ getX() : int
	+ getY() : int
}

class CaseMur{
	- positionX : int
	- positionY : int
	+ CaseMur(x : int, y : int)
    + getX() :int
	+ getY() : int
	+ getType() :  String
}

class CaseVide{
	- positionX : int
	- positionY : int
	+ CaseVide(x : int, y : int)
    + getX() :int
	+ getY() : int
	+ getType() : String
}

interface Jeu{
	+ evoluer(Commande)
	+ etreFini() : boolean
}

interface DessinJeu{
	+ dessiner(Bufferedimage)
}

class LabyrintheGraphique{
	+ dessiner(Bufferedimage)
}

abstract class Monstre<<Nouv>>{
	
}

class MonstreImmobile<<Nouv>>{
	
}

interface Objet<<Nouv>>{
	+ afficheElement()
}

class Inventaire<<Nouv>>{
	+ ajoutElementInventaire(Objet o)
	+ afficheContenuInventaire()
}

Personnage <|-- Aventurier
Personnage <|-- Monstre
Monstre <|-- MonstreImmobile
Aventurier "1" <-- "1" Labyrinthe : -aventurier
CaseLab "*" <-- "1" Labyrinthe : -tab_cases
CaseLab <|-- CaseMur
CaseLab <|-- CaseVide
CaseVide "1"<-- "1"Labyrinthe : -depart
Jeu <|.. Labyrinthe
Labyrinthe "1" <-- "1" LabyrintheGraphique : -labyrinthe
DessinJeu <|.. LabyrintheGraphique
Monstre "*" <-- "1" Labyrinthe : -listeMonstres
Objet "*" <-- "1" Inventaire : -listeObjets
Inventaire "1" <-- "1" Aventurier : -inventaire
@enduml